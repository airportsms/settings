<?php

namespace Airportsms\Settings\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel as Sentinel;

class Setting extends Model
{
    protected $fillable = ['key', 'value'];
    protected $appends = ['user_val'];

    //Big block of caching functionality.
    public $with = ['userSetting'];

    public function userSetting(){
        return $this->belongsToMany('App\User')->withPivot('user_value');
    }
    public function getUserValAttribute()
    {
        if ($this->userSetting()->wherePivot('user_id', Sentinel::getUser()->id)->first()) {
            return $this->userSetting()->wherePivot('user_id', Sentinel::getUser()->id)->first()->pivot->user_value;
        } else {
            return null;
        }
    }
    public function cached()
    {
        // check cache is available else set cache and return
        return Cache::get('Airportsms\Settings\Setting', function () {
            $data = $this->get();
            Cache::put('Airportsms\Settings\Setting', $data, 60 * 24);

            return $data;
        });
    }
    public function save(array $options = [])
    {   //both inserts and updates
        parent::save($options);
        
        // delete cache
        Cache::forget('Airportsms\Settings\Setting');
        //dd(Cache::get('Airportsms\Settings\Setting'));

    }
    public function delete(array $options = [])
    {   //soft or hard
        parent::delete($options);


        // delete cache
        Cache::forget('Airportsms\Settings\Setting');
    }

    public static function boot()
    {
        parent::boot();    
    
        // Egy beállítás törlésénél a felhasználó beállítását is törli
        static::deleted(function($setting)
        {
            $setting->userSetting()->delete();
        });
    }    

    public function restore()
    {   //soft delete undo's
        parent::restore();

        // delete cache
        Cache::forget('Airportsms\Settings\Setting');
    }
}

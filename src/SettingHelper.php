<?php

namespace Airportsms\Settings;

use Illuminate\Support\Facades\Cache;
use Airportsms\Settings\Models\Setting as SettingModel;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel as Sentinel;

/**
 * Setting class is helper class to get set all settings
 * from/to database
 */
class SettingHelper
{
    /**
     * Check setting has key exits
     *
     * @param  string  $key
     * @return boolean
     */
    public function has($key, $user = null){   
        $isHas = (new SettingModel)
                ->cached()
                ->where('key', $key)
                ->count() > 0;
        $loggedUser = Sentinel::getUser();
        //dd($loggedUser);
        if($user and $isHas and $loggedUser){
            return (new SettingModel)
                ->cached()
                ->where('key', $key)
                ->first()
                ->userSetting()
                ->wherePivot('user_id', $loggedUser->id)
                ->count() > 0;
        }else{
            return $isHas;
        }
    }

    /**
     * Get value if key exists else it will return null
     *
     * @param  string $key
     * @return string|null
     */
    public function get($key, $user = null)
    {
        if (!$this->has($key)) {
            return null;
        }
        //dd((new SettingModel)->where('key', "lang")->first()->user_val);

        $loggedUser = Sentinel::getUser();
        if ($user and $loggedUser){
            $user_setting = (new SettingModel)
            ->cached()
            ->where('key', $key)
            //->first()->userSetting()->wherePivot('user_id', Sentinel::getUser()->id)->first();
            ->first()->user_val;
            if ($user_setting){
                return $user_setting;
            }else{
                return (new SettingModel)
                ->cached()
                ->where('key', $key)
                ->first()->value;
            }
        }else{
            return (new SettingModel)
            ->cached()
            ->where('key', $key)
            ->first()->value;
        }
        
    }

    /**
     * Store new key and value in settings if it's already
     *  exits then override.
     *
     * @param  string $key
     * @param  string $value
     * @return null
     */
    public function put($key, $value, $user = null)
    {
        // if key exits then override
        $isHas = $this::has($key);

        if ($user and $isHas) {
            if ($this::has($key, $user)) {
            $setting = (new SettingModel)
                ->cached()
                ->where('key', $key)
                ->first()->userSetting()->updateExistingPivot(Sentinel::getUser()->id, ['user_value' => $value]);
                Cache::forget('Airportsms\Settings\Setting');
            return $setting;
            }
            Cache::forget('Airportsms\Settings\Setting');
            return SettingModel::where('key', $key)->first()->userSetting()->attach([Sentinel::getUser()->id => ['user_value' => $value]]);

        }else{
            if ($isHas) {
            $setting = (new SettingModel)
                ->cached()
                ->where('key', $key)
                ->first();

            $setting->value = $value;
            Cache::forget('Airportsms\Settings\Setting');
            return $setting->save();
            }

            return SettingModel::create([
                'key' => $key,
                'value' => $value,
            ]);
        }


        
    }

    /**
     * Store new key and value in settings if it's already
     *  exits then override.
     *
     * @param  string $key
     * @param  string $value
     * @return null
     */
    public function set($key, $value, $user = null)
    {
        return $this->put($key, $value, $user);
    }

    /**
     * Delete setting data from databasse
     *
     * @param  string $key
     * @return boolean
     */
    public function forget($key, $user=null)
    {
        if ($user) {
            $isDeleted = SettingModel::where('key', $key)->first()->userSetting()->detach(Sentinel::getUser()->id);
        }else{
            $isDeleted = SettingModel::where('key', $key)
                ->delete();
    
            // if delete record then delete cache
            if ($isDeleted) {
                Cache::forget('Airportsms\Settings\Setting');
            }
        }  
        return $isDeleted;
    }
}
